from django.db import models
from django.db.models.base import Model
from django import forms

# Create your models here.


class Users(models.Model):
    user_name = models.CharField(max_length = 100)
    password = models.CharField(max_length = 20, widget = forms.PasswordInput)
    first_name = models.CharField(max_length = 100)
    last_name = models.CharField(max_length = 100)
    email_address = models.EmailField()
    profile_pic = models.ImageField(blank = True, null = True)
    gender = models.CharField()
    mobile_number = models.CharField(max_length = 10)
    

class Addresses(models.Model):
    user_id = models.ForeignKey()
    home_street_no = models.IntegerField()
    city = models.CharField()
    state = models.CharField()
    country = models.CharField()
    zip_code = models.IntegerField()
    

class Orders(models.Model):
    product_id = models.ForeignKey()
    user_id = models.ForeignKey()
    quantity = models.IntegerField()
    order_date = models.DateField()
    status = models.CharField()
    
    
class Carts(models.Model):
    user_id = models.ForeignKey()
    product_id = models.ForeignKey()
    quantity = models.IntegerField()