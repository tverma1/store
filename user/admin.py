from django.contrib import admin
from user.models import Users, Addresses, Orders, Carts

# Register your models here.

@admin.register(Users)
class UsersAdmin(admin.ModelAdmin):
    list_display = ('user_name', 'password', 'first_name', 'last_name', 'email_address', 'profile_pic', 'gender', 'mobile_number')


@admin.register(Addresses)
class AddressesAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'home_street_no', 'city', 'state', 'country', 'zip_code')

@admin.register(Orders)
class OrdersAdmin(admin.ModelAdmin):
    list_display = ('product_id', 'user_id', 'quantity', 'order_date', 'status')
    
@admin.register(Carts)
class CartsAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'product_id', 'quantity')