from django.db import models
from django.db.models.fields import IntegerField

# Create your models here.

class Departments(models.Model):
    Department_name = models.CharField(max_length = 100)
 
class Jobs(models.Model):
    department_id = models.ForeignKey(Departments)
    job_title = models.CharField(max_length=100)
   

class Employees(models.Model):
    #user_id = models.ForeignKey(Users)
    Department_id = models.ForeignKey(Departments)
    job_id = models.ForeignKey(Jobs)
    salary = models.IntegerField()
    DOJ = models.DateField()
    DOB = models.DateField()
    
class Attendances(models.Model):
    emp_id = models.ForeignKey()
    work_day = models.IntegerField()
    check_in = models.TimeField()
    check_out = models.TimeField()
       

