from django.contrib import admin
from employee.models import Departments, Jobs, Employees, Attendances

# Register your models here.

@admin.register(Departments)
class DepartmentsAdmin(admin.ModelAdmin):
    list_display = ('Department_name',)

@admin.register(Jobs)
class JobsAdmin(admin.ModelAdmin):
    list_display = ('department_id', 'job_title')
    
@admin.register(Employees)
class EmployeesAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'Department_id', 'job_id', 'salary', 'DOJ', 'DOB')

@admin.register(Attendances)
class AttendancesAdmin(admin.ModelAdmin):
    list_display = ('emp_id', 'work_day', 'check_in', 'check_out')
