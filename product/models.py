from django.db import models
from django.db.models.fields import CharField, IntegerField

# Create your models here.


class Coupons(models.Model):
    coupon_code = models.IntegerField()
    description = models.CharField(max_length = 500)
    start_date = models.DateField()
    end_date = models.DateField()


class Categories(models.Model):
    name = models.CharField(max_length = 100) 
    picture = models.ImageField()
    description = models.CharField(max_length = 500)


class Products(models.Model):
    #coupon_id = models.ForeignKey(Coupons, blank = True, null = True)
    #category_id = models.ForeignKey(Categories)
    name = models.CharField(max_length = 100) 
    picture = models.ImageField()
    rating = models.DecimalField(default = 00000.0)
    description = models.CharField(max_length = 500)
    MFD = models.DateField(blank = True, null = True)
    price = models.IntegerField()
    
