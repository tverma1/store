from django.contrib.admin.decorators import register
from product.models import Coupons, Categories, Products
from django.contrib import admin

# Register your models here.


@admin.register(Coupons)
class CouponsAdmin(admin.ModelAdmin):
    list_display = ('coupon_code', 'description', 'start_date', 'end_date')

@admin.register(Categories)
class CategoriesAdmin(admin.ModelAdmin):
    list_display = ('name', 'picture', 'description')
    
@admin.register(Products)
class ProductsAdmin(admin.ModelAdmin):
    list_display = ('coupon_id', 'category_id', 'name', 'picture', 'rating', 'description', 'MFD', 'price')
    